#include <iostream>
#include <exception>

using namespace std;

int testaExcecao(void){

	int valor;
	cout << "Insira um valor: ";
	cin >> valor;

	if(valor <= 0){
		throw(5);
	}
	else 
		return valor;
}

int main(int argc, char const *argv[])
{
	int valor;
	try{
		valor = testaExcecao();
		cout << "Valor = " << valor << endl;
	}
	catch(int v){
		cout << "Exceção capturada" << endl;
	}
	catch(char a)
	{
		cout << "Exceção Capturada do tipo CHAR" << endl;
	}
	catch (...)
	{
		cout << "Exceção Capturada do tipo GENERICA" << endl;
	}
	
	return 0;
}